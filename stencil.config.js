exports.config = {
  namespace: 'riovircomponents',
  generateDistribution: true,

  bundles: [
    { components: [
      'riovir-icon',
      'riovir-jenkins-job',
      'riovir-name',
      'riovir-metric',
      'xui-accordion',
      'xui-accordion-title',
      'xui-accordion-content',
      'xui-checkbox',
      'xui-container',
      'xui-dropdown',
      'xui-dropdown-item',
      'xui-list',
      'xui-list-item',
      'xui-menu',
      'xui-segment'
    ] }
  ],
  collections: [
    { name: '@stencil/router' }
  ]
};

exports.devServer = {
  root: 'www',
  watchGlob: '**/**'
}
