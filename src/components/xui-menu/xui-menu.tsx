import { Component, Element, Method, Listen, Prop, State, PropDidChange } from '@stencil/core';

const ITEM_NAME = 'xui-menu-item';
const TAB_NAME = 'xui-segment';

enum Direction {
  Top = 'top',
  Bottom = 'bottom'
}

@Component({
  tag: 'xui-menu',
  styleUrl: 'xui-menu.scss',
})
export class Menu {
  @Element() el: HTMLElement;

  // Types
  @Prop() attach: any;
  @Prop() tabular: boolean = false;

  context: HTMLElement;

  @Listen('itemActivated')
  onItemActivated(event) {
    const activeItem = event.detail;
    const items = this.el.querySelectorAll(ITEM_NAME);
    Array.prototype.forEach.call(items, item => { item.active = item === activeItem; });
    if (activeItem['dataTab']) {
      this.activateTab(activeItem['dataTab']);
    }
  }

  @Method()
  setContext(context: HTMLElement) {
    this.context = context;
  }

  componentDidLoad() {
    this.el.classList.add('ui', 'menu');
    if (!this.el.tabIndex) { this.el.tabIndex = 0; }
    this.updateTabular(this.tabular);
    this.updateAttach(this.attach);
  }

  @PropDidChange('attach')
  updateAttach(value) {
    this.el.classList.remove('top', 'bottom', 'attached');
    if (value === Direction.Top) { this.el.classList.add('top'); }
    if (value === Direction.Bottom) { this.el.classList.add('bottom'); }
    if (value !== undefined) { this.el.classList.add('attached'); }
  }

  @PropDidChange('tabular')
  updateTabular(value) { this.setClass('tabular', value); }

  activateTab(dataTab: string) {
    const context = this.context || document;
    const tabs = context.querySelectorAll(`${TAB_NAME}[data-tab]`);
    Array.prototype.forEach.call(tabs, tab => { tab.active = tab.dataTab === dataTab; });
  }

  setClass(name: string, value: boolean = true) {
    const update = value ? 'add' : 'remove';
    this.el.classList[update](name);
  }
}
