import { Component, Element, Prop, PropDidChange } from '@stencil/core';

@Component({
  tag: 'xui-accordion-content'
})
export class AccordionContent {
  @Element() el: HTMLElement;

  @Prop() active: boolean;

  componentDidLoad() {
    this.el.classList.add('content');
    this.updateActive(this.active);
  }

  @PropDidChange('active')
  updateActive(active: boolean) {
    const update = this.active ? 'add' : 'remove';
    this.el.classList[update]('active');
  }
}
