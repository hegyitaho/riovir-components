import { Component, Element, Prop, PropDidChange, Listen } from '@stencil/core';

@Component({
  tag: 'xui-dropdown-item'
})
export class DropdownItem {
  @Element() el;

  @Prop() text: string;
  @Prop() value: any;
  @Prop() active: boolean = false;

  componentDidLoad() {
    this.el.classList.add('item');
    this.updateActive(this.active);
  }

  @PropDidChange('active')
  updateActive(active: boolean) {
    const update = this.active ? 'add' : 'remove';
    this.el.classList[update]('active');
    this.el.classList[update]('selected');
  }

  render() {
    return this.text;
  }
}
