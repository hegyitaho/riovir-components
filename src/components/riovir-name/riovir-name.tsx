import { Component, Prop, PropWillChange, PropDidChange } from '@stencil/core';


@Component({
  tag: 'riovir-name',
  styleUrl: 'riovir-name.scss'
})
export class Name {

  @Prop() first: string;

  @Prop() last: string;

  @PropWillChange('first')
  willChangeHandler(value: string) {
    console.log('will', value);
  }

  @PropDidChange('last')
  didChangeHandler(value: string) {
    console.log('did', value);
  }

  render() {
    return (
      <div>
        Hello, my name is {this.first} {this.last}
      </div>
    );
  }
}
