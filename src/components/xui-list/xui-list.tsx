import { Component, Element, Prop, PropDidChange } from '@stencil/core';


@Component({
  tag: 'xui-list'
})
export class MyName {
  @Prop() relaxed: boolean;
  @Prop() divided: boolean;

  render() {
    const classes = {
      ui: true,
      list: true,
      relaxed: this.relaxed,
      divided: this.divided
    };
    return <div class={classes}>
      <slot></slot>
    </div>
  }
}
